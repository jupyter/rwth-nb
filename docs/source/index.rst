Welcome to RWTH Notebooks's documentation!
==========================================

Enhancing Jupyter Notebooks for RWTH Aachen.

The core functions implemented here are:

.. toctree::
  :maxdepth: 2
  :caption: Contents:

  user_guide
  api
    
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
