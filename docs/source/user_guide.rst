User Guide
===========

.. toctree::
  :maxdepth: 3

  examples/RWTH Plots.ipynb
  examples/RWTH Colors.ipynb
  examples/RWTH Misc.ipynb
  examples/RWTH Widgets.ipynb
