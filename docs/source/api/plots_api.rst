*******************
``rwth_nb.plots``
*******************

.. contents:: Table of Contents
   :depth: 3


RWTH Colors
================

.. automodule:: rwth_nb.plots.colors
  :members:


Matplotlib Decorations
================

.. automodule:: rwth_nb.plots.mpl_decorations
  :members:

