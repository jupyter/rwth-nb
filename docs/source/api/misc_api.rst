*******************
``rwth_nb.misc``
*******************

.. contents:: Table of Contents
   :depth: 3


Feedback
================

.. automodule:: rwth_nb.misc.feedback
  :members:


Media
================

.. automodule:: rwth_nb.misc.media
  :members:

Signals
================
.. automodule:: rwth_nb.misc.signals
  :members:


Filters
================

.. automodule:: rwth_nb.misc.filters
  :members:


Transforms
================

.. automodule:: rwth_nb.misc.transforms
  :members: